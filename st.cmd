#!/usr/bin/env iocsh.bash

################################################################
### requires
require(lakeshore155)

### lakeshore155 (ls1)
epicsEnvSet("IPADDR",               "172.30.244.177")
epicsEnvSet("IPPORT",               "8888")
epicsEnvSet("LOCATION",             "Utgard: $(IPADDR):$(IPPORT)")
epicsEnvSet("SYS",                  "UTG-SEE")
epicsEnvSet("P",                    "$(SYS):")
epicsEnvSet("DEV",                  "LS155-001")
epicsEnvSet("R",                    "$(DEV):")
epicsEnvSet("PORTNAME",             "$(SYS)-$(DEV)")
epicsEnvSet("A",                    -1)

### E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

### load all db's
iocshLoad("$(lakeshore155_DIR)lakeshore155.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORTNAME=$(PORTNAME), A=$(A)")

